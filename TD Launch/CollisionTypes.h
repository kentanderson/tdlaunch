//
//  CollisionTypes.h
//  TD Launch
//
//  Created by Kent Anderson on 9/8/12.
//
//

#ifndef TD_Launch_CollisionTypes_h
#define TD_Launch_CollisionTypes_h

#import <Foundation/Foundation.h>

extern const NSString* CT_Character;
extern const NSString* CT_LaunchPad;
extern const NSString* CT_LaunchPadSegment;
extern const NSString* CT_NoBottomCollision;
extern const NSString* CT_Target;
extern const NSString* CT_Blower;
extern const NSString* CT_BlowerBody;
extern const NSString* CT_Trampoline;
extern const NSString* CT_Slide;
extern const NSString* CT_Plank;
extern const NSString* CT_Gem;
extern const NSString* CT_Sensor;
extern const NSString* CT_NamedWall;
extern const NSString* CT_Boulder;
extern const NSString* CT_Launcher;
extern const NSString* CT_Projectile;
extern const NSString* CT_Projectile_Bird;
extern const NSString* CT_Projectile_Plank;
extern const NSString* CT_Projectile_Slide;
extern const NSString* CT_AreaSensor;
extern const NSString* CT_Accelerator;

#endif
