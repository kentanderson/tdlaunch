//
//  CollisionTypes.m
//  TD Launch
//
//  Created by Kent Anderson on 9/8/12.
//
//

#import "CollisionTypes.h"


const NSString* CT_Character = @"character";
const NSString* CT_LaunchPad = @"launchpad";
const NSString* CT_LaunchPadSegment = @"launchpadsegment";
const NSString* CT_NoBottomCollision = @"nobottomcollision";
const NSString* CT_Target = @"target";
const NSString* CT_Blower = @"blower";
const NSString* CT_BlowerBody = @"blowerbody";
const NSString* CT_Trampoline = @"drum";
const NSString* CT_Slide = @"slide";
const NSString* CT_Plank = @"plank";
const NSString* CT_Gem = @"gem";
const NSString* CT_Sensor = @"sensor";
const NSString* CT_NamedWall = @"namedwall";
const NSString* CT_Boulder = @"boulder";
const NSString* CT_Launcher = @"launcher";
const NSString* CT_Projectile = @"projectile";
const NSString* CT_Projectile_Bird = @"projectile_bird";
const NSString* CT_Projectile_Plank = @"projectile_plank";
const NSString* CT_Projectile_Slide = @"projectile_slide";
const NSString* CT_AreaSensor = @"area_sensor";
const NSString* CT_Accelerator = @"accelerator";


