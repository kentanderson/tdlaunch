//
//  ProductIds.h
//  TD Launch
//
//  Created by Kent Anderson on 2/28/14.
//
//

#import <Foundation/Foundation.h>


extern NSString* const Product_SuperStartPack;
extern NSString* const Product_PowerPack;
extern NSString* const Product_DevPack;

extern NSString* const Tool_Plank;
extern NSString* const Tool_Slide;
extern NSString* const Tool_Drums;
extern NSString* const Tool_Blowers;
extern NSString* const Tool_Reducer;
extern NSString* const Tool_Doubler;
extern NSString* const Tool_Aligner;
extern NSString* const Tool_Launcher;
extern NSString* const Tool_GravityReverser;

