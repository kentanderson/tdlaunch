//
//  ProductIds.m
//  TD Launch
//
//  Created by Kent Anderson on 2/28/14.
//
//

#import "ProductIds.h"


NSString* const Product_SuperStartPack = @"com.kornerstoane.tdlaunch.SuperStartPack";
NSString* const Product_PowerPack = @"com.kornerstoane.tdlaunch.PowerPack";
NSString* const Product_DevPack = @"com.kornerstoane.tdlaunch.DevPack";

NSString* const Tool_Plank = @"tool.plank";
NSString* const Tool_Slide = @"tool.slide";
NSString* const Tool_Drums = @"tool.drums";
NSString* const Tool_Blowers = @"tool.blowers";
NSString* const Tool_Reducer = @"tool.reducer";
NSString* const Tool_Doubler = @"tool.doubler";
NSString* const Tool_Aligner = @"tool.aligner";
NSString* const Tool_Launcher = @"tool.launcher";
NSString* const Tool_GravityReverser = @"tool.gravityreverser";