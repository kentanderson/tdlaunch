//
//  RulerNode.h
//  TD Launch
//
//  Created by Kent Anderson on 8/25/12.
//
//

#import "cocos2d.h"

@interface RulerNode : CCNode

@property int majorTicks;
@property CGSize fontSize;
@property ccColor4F fgColor;
@property ccColor4F bgColor;

@end