//
//  main.m
//  TD Launch
//
//  Created by Kent Anderson on 8/7/12.
//  Copyright Kornerstone Creativity, LLC, 2012. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[])
{
    int retVal = UIApplicationMain(argc, argv, nil, @"AppController");
    return retVal;
}
